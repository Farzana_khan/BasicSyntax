<?php

$foo = "0";  // $foo is string (ASCII 48)
$foo += 2;   // $foo is now an integer (2)
echo $foo;
echo "<br>";
$foo = $foo + 1.3;  // $foo is now a float (3.3)
$foo = 5 + "10 Little Piggies"+3+"little 4 pc"; // $foo is integer (15)
echo $foo;
echo "<br>";
$foo = 5 + "10 Small Pigs";     // $foo is integer (15)

echo "<hr>";
echo "Type Conversion";

$foo=true;
$foo=(int)$foo;
echo "<br>";
echo gettype($foo );

$foo=5;
$foo=(boolean)$foo;
echo "<br>";
echo gettype($foo );

$foo=5;
$foo=(string)$foo;
echo "<br>";
echo gettype($foo );

?>